#include "ofApp.h"
// #define MODERN

//--------------------------------------------------------------
void ofApp::setup()
{
    sampleRate = 48000;
    bufferSize = 1024;

    ofSoundStreamSettings settings;
    settings.setOutListener(ofGetAppPtr());
    settings.sampleRate = sampleRate;
    settings.bufferSize = bufferSize;
    settings.numOutputChannels = 2;
    settings.numBuffers = 2;
    // soundStream.setup(settings);
    ofBackground(0, 0, 0);
    // load our image inside bin/data
    image.load("pollock.jpg");
    // image.load("blob.png");
    // if the app performs slowly raise this number
    sampling = 2;

    // store width and height for optimization and clarity
    int w = (int)image.getWidth();
    int h = (int)image.getHeight();

    // offsets to center the particle son screen
    int xOffset = (ofGetWidth() - w) / 2;
    int yOffset = (ofGetHeight() - h) / 2;

#ifdef MODERN
    texture = image.getTexture();
    shader.load("shader.vert", "shader.frag");
    shader.begin();
    shader.setUniform1f("friction", friction);
    shader.setUniform1f("springFactor", springFactor);
    shader.setUniform1f("forceRadius", forceRadius);
    shader.setUniformTexture("tex0", texture, 0);
    shader.bindAttribute(4, "velocity");
    shader.bindAttribute(5, "acceleration");
    shader.end();
#endif // DEBUG

    for (int x = 0; x < w; x += sampling) {
        for (int y = 0; y < h; y += sampling) {
            int index = (y * w + x) * 3;
#ifdef MODERN
            const glm::vec2 current_coord = texture.getCoordFromPoint(x, y);
            // ofColor color;
            glm::vec2 velocity(0.f, 0.f);
            glm::vec2 acceleration(0.f, 0.f);
            // color.r = image.getPixels()[index];
            // color.g = image.getPixels()[index + 1]; // blue pixel
            // color.b = image.getPixels()[index + 2]; // green pixel
            glm::vec3 currentPoint = glm::vec3(x + xOffset, y + yOffset, 0);
            VBO.setVertexData(&currentPoint, 3, GL_STATIC_DRAW);
            VBO.setTexCoordData(&current_coord, 2, GL_STATIC_DRAW);

            glBindVertexArray(VBO.getVaoId());
            VBO.bind();

            glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(glm::vec2), &velocity, GL_DYNAMIC_DRAW);
            glVertexAttribPointer(shader.getAttributeLocation("velocity"), 2, GL_FLOAT, GL_FALSE, sizeof(float), (void*)nullptr);
            glEnableVertexAttribArray(shader.getAttributeLocation("velocity"));
            glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(glm::vec2), &acceleration, GL_DYNAMIC_DRAW);
            glVertexAttribPointer(shader.getAttributeLocation("acceleration"), 2, GL_FLOAT, GL_FALSE, sizeof(float), (void*)nullptr);
            glEnableVertexAttribArray(shader.getAttributeLocation("acceleration"));
            VBO.unbind();

            // VBO.setAttributeData(shader.getAttributeLocation("velocity"), &velocity, 1, 1, GL_SHADER_STORAGE_BUFFER);
            // VBO.setAttributeData(shader.getAttributeLocation("acceleration"), &acceleration, 1, 1, GL_SHADER_STORAGE_BUFFER);
            // particles.push_back(Particle(glm::vec3(x + xOffset, y + yOffset, 0), color));

#else
            ofColor color;
            color.r = image.getPixels()[index];
            color.g = image.getPixels()[index + 1]; // blue pixel
            color.b = image.getPixels()[index + 2]; // green pixel
            particles.push_back(Particle(ofPoint(x + xOffset, y + yOffset), color));
#endif // MODERN
        }
    }

    // ofSetFrameRate(30);
    numParticles = (image.getWidth() * image.getHeight()) / sampling;

    // Set spring and sink values
    cursorMode = 0;
    forceRadius = 45;
    friction = 0.85;
    springFactor = 0.12;
    springEnabled = true;
    type = GL_POINTS;
}

//--------------------------------------------------------------
void ofApp::update()
{
#ifndef MODERN
    ofPoint diff; // Difference between particle and mouse
    float dist; // distance from particle to mouse ( as the crow flies )
    float ratio; // Ratio of how strong the effect is = 1 + (-dist/maxDistance) ;
    const ofPoint mousePosition = ofPoint(mouseX, mouseY); // Allocate and retrieve mouse values once.

    // Create an iterator to cycle through the vector
    std::vector<Particle>::iterator p;
    for (p = particles.begin(); p != particles.end(); p++) {
        ratio = 1.0f;
        p->velocity *= friction;
        // reset acceleration every frame
        p->acceleration = ofPoint();
        diff = mousePosition - p->position;
        dist = ofDist(0, 0, diff.x, diff.y);
        // If within the zone of interaction
        if (dist < forceRadius) {
            ratio = -1 + dist / forceRadius;
            // Repulsion
            if (cursorMode == 0)
                p->acceleration -= (diff * ratio);
            // Attraction
            else
                p->acceleration += (diff * ratio);
        }
        if (springEnabled) {
            // Move back to the original position
            p->acceleration.x += springFactor * (p->spawnPoint.x - p->position.x);
            p->acceleration.y += springFactor * (p->spawnPoint.y - p->position.y);
        }

        p->velocity += p->acceleration * ratio;
        p->position += p->velocity;
    }
#endif
}

//--------------------------------------------------------------
void ofApp::draw()
{
#ifdef MODERN
    shader.begin();
    shader.setUniform2f("MousePos", glm::vec2(mouseX, mouseY));
    shader.setUniform1i("cursorMode", cursorMode);
    shader.setUniform1i("springEnabled", springEnabled);
    VBO.draw(type, 0, VBO.getNumVertices());
    shader.end();
#else
    glBegin(type);

    std::vector<Particle>::iterator p;
    for (p = particles.begin(); p != particles.end(); p++) {
        glColor3ub((unsigned char)p->color.r, (unsigned char)p->color.g, (unsigned char)p->color.b);
        glVertex3f(p->position.x, p->position.y, 0);
    }
    glEnd();

#endif // DEBUG
    ofSetColor(255, 255, 255);

    string output = "S :: Springs on/off : " + ofToString(springEnabled) +

        "\n C :: CursorMode repel/attract " + ofToString(cursorMode) +

        "\n # of particles : " + ofToString(numParticles) +

        " \n fps:" + ofToString(ofGetFrameRate());

    ofDrawBitmapString(output, 20, 666);
}

//--------------------------------------------------------------
void ofApp::audioOut(ofSoundBuffer& buffer)
{
    float currentSample = 0;
    for (size_t i = 0; i < buffer.getNumFrames(); i++) {
        buffer[i * buffer.getNumChannels()] = currentSample;
        buffer[i * buffer.getNumChannels() + 1] = currentSample;
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{

    switch (key) {
    case 'c':
    case 'C':
        cursorMode = (cursorMode + 1 > 1) ? 0 : 1;
        break;

    case 's':
    case 'S':
        springEnabled = !springEnabled;
        break;
    case '1':
        type = GL_POINTS;
        break;
    case '2':
        type = GL_TRIANGLES;
        break;
    case '3':
        type = GL_TRIANGLE_STRIP;
        break;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) { }

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) { }

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) { }

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) { }

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) { }

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) { }

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) { }

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) { }

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) { }

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) { }

//--------------------------------------------------------------
void ofApp::exit()
{
    // soundStream.close();
}
