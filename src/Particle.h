#pragma once

#include "ofMain.h"

// A class to abstract the co-ordinates into particle system properties
class Particle {
public:
    Particle();

    // Store location of pixels, their colors and their initual location
    Particle(const glm::vec3& _position, ofColor _color)
        : position(_position)
        , spawnPoint(_position)
        , color(_color)
    {
        velocity = ofPoint(ofRandom(-5, 5), ofRandom(-5, 5));
    }

    glm::vec3 position, velocity;
    glm::vec3 acceleration; // smoothing applied to velocity
    glm::vec3 spawnPoint; // original location to line up the picture
    ofColor color;
};
