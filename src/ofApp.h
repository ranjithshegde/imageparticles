#pragma once

#include "Particle.h"

class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();
    void audioOut(ofSoundBuffer& buffer);
    void exit();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    ofSoundStream soundStream;
    int sampleRate;
    int bufferSize;

    ofImage image; // image to load
    int sampling; // pixels to skip
    vector<Particle> particles; // vector to store pixels
    int numParticles; // Number of particles

    // Spring and Sink Factors
    bool springEnabled; // toggle whether particles return to their origin
    float forceRadius; // radius of repellent/attraction force
    float friction; // damping to slow the particles down
    float springFactor; // how much the particle "springs" back to origin
    int cursorMode;
    GLuint type;

#ifdef MODERN
    ofVbo VBO;
    ofTexture texture;
    ofShader shader;
#endif
};
