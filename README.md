# Particle system from Images

A simple project that converts the texture co-ordinates of an image into a live
particle system

A simple particle system structure is defined as follows

```cpp
// A class to abstract the co-ordinates into particle system properties
class Particle {
public:
    Particle();

    // Store location of pixels, their colors and their initual location
    Particle(const glm::vec3& _position, ofColor _color)
        : position(_position)
        , spawnPoint(_position)
        , color(_color)
    {
        velocity = ofPoint(ofRandom(-5, 5), ofRandom(-5, 5));
    }

    glm::vec3 position, velocity;
    glm::vec3 acceleration; // smoothing applied to velocity
    glm::vec3 spawnPoint; // original location to line up the picture
    ofColor color;
};
```

Each of these particles are updated once per frame in `ofApp::update()` call
using the mouse location

```cpp
 if (springEnabled) {
   p->velocity *= friction;
   // reset acceleration every frame
   p->acceleration = ofPoint();
   diff = mousePosition - p->position;
   dist = ofDist(0, 0, diff.x, diff.y);
   // If within the zone of interaction
   if (dist < forceRadius) {
       ratio = -1 + dist / forceRadius;
       // Repulsion
       if (cursorMode == 0)
           p->acceleration -= (diff * ratio);
       // Attraction
       else
           p->acceleration += (diff * ratio);
   }
     // Move back to the original position
   p->acceleration.x += springFactor * (p->spawnPoint.x - p->position.x);
   p->acceleration.y += springFactor * (p->spawnPoint.y - p->position.y);
 }

 p->velocity += p->acceleration * ratio;
 p->position += p->velocity;
```

The particle system can either by individual sprites (points with texture), they
can form consequent distinct trianges starting from origin to mouse movements,
or the triangles can be arranges as fans.
These settings are toggled using keys `1`, `2` & `3`

Additionally a spring force can be applied that oscillates the location of the
points between a specified previous and current location.
